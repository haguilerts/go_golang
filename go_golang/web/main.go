package main

import (
	"fmt"
	"strconv"
)

/**  *********declaracion de variables glovales***********
var numero int// entero
var texto string // texto
var satus bool // booleans
int8 int16 int32 int64
float32 float64
uint   uint8 uint16 uint32 uint64 // varibales sin signos
// asignamos variables
numero := 2020
texto:="hola soy Giovanny" **/

func clase1() {
	/////////////////////////// inicio ///////////////////////////////
	fmt.Println("************** clase 1 *******************")

	var numero int = 50 // declaro y asigno un valor, en esta ocupa un espacio de memoria
	numero1 := 2020     // solo se puede asignarlo y declara una unica ves
	texto := "hola soy giovanny"
	numero = 500 // asigno otro valor
	var satus bool = true
	fmt.Println("hola mundo")
	fmt.Println(numero1)
	fmt.Println(numero)
	fmt.Println(texto)
	fmt.Println(satus)

	fmt.Println("************** THEN END *******************")
	//////////////////////////////////////////////////////////
}
func clase2() {
	////////////////////////////// asignacion y declaracion ////////////////////////////
	fmt.Println("************** clase 2 *******************")

	var persona, edad, sexo, mail string
	persona, edad, sexo, mail, anio := " juan- ", " 25 años- ", " masculino- ", " gio@gmail.com", 2015
	var dni int = 94404758

	fmt.Println(persona + "carlos")
	fmt.Println(" tiene " + edad + "de sexo " + sexo)
	fmt.Println(" con un mail:" + mail)
	fmt.Println(anio)
	//fmt.Println( " anio: "+anio ) // aca no me deja concatenar porque no puedo mesclar un string con int para imprimir
	fmt.Println(dni)

	fmt.Println("************** THEN END *******************")
	//////////////////////////////////////////////////////////
}
func clase3() {
	///////////////////////////// COVERCION DE VARIABLES /////////////////////////////
	fmt.Println("************** clase 2 *******************")
	// int ==> string
	edad := 22
	edad_str := strconv.Itoa(edad)
	fmt.Println("mi edad es: " + edad_str)

	// string ==> int
	anio := "2020"
	anio_int, err := strconv.Atoi(anio) // este metodo retorna dos valores, en la primera se gusrda el calor esperado y en el otro "una etiqueda <nil>"
	fmt.Println(10 + anio_int)          // aca no concatena sinos q suma
	fmt.Println(err)                    // muestra <nil>  ??

	fmt.Println("************** THEN END *******************")
	//////////////////////////////////////////////////////////
}
func clase4() {
	/////////////////////////////  condicionales  /////////////////////////////

	/** operaciones
	== igual, != desigual, >mayor, <menor, >=mayor e Igual
	&& and(si todo es verdadero), || or(algunas son verdaderas)
	**/
	x := 10
	y := 5
	z := 10
	// las condiciones del IF puede o no ir con parectesis
	if x > y && z == x {
		fmt.Println("correcto...")
		fmt.Println(x, " es mayor que ", y)
	} else {
		fmt.Println("falso")
	}

	fmt.Println("************** THEN END *******************")
	//////////////////////////////////////////////////////////
}
func clase5() {
	///////////////////////////// ciclos  /////////////////////////////

	for i := 0; i < 10; i++ {
		fmt.Println("pos1: ", i)
	}
	fmt.Println("\n")
	for j := 2; j <= 20; j = j + 2 {
		fmt.Println("pos2: ", j)
	}
	fmt.Println("\n")
	k := 0
	for k <= 40 {
		fmt.Println("pos3: ", k)
		k = k + 4
	}
	fmt.Println("************** THEN END *******************")
	//////////////////////////////////////////////////////////
}
func clase6() {
	/////////////////////////////  Arreglos  /////////////////////////////
	// vat nombre [] int
	var arreglo_int [5]int
	var arreglo_str [5]string
	fmt.Println(arreglo_int, arreglo_str)

	arreglo := [3]int{2, 4}
	fmt.Println("cantidad:", len(arreglo))
	//arreglo[2] := 50
	for i := 0; i < len(arreglo); i++ {
		fmt.Println(arreglo[i])
	}

	var matriz [3][3]int
	matriz[0][0] = 0
	matriz[0][1] = 1
	matriz[0][2] = 2
	matriz[1][0] = 3
	matriz[1][1] = 4
	matriz[1][2] = 5
	matriz[2][0] = 6
	matriz[2][1] = 7
	matriz[2][2] = 8
	fmt.Print(matriz, "\n")

	fmt.Println("************** THEN END *******************")
	//////////////////////////////////////////////////////////
}
func clase7() {
	/////////////////////////////  Slices  /////////////////////////////
	/**slice es:
	similar a un array pero la diferecncia es no posee un tamaño fijo sino q variable(dinamico).
	los Slices se caracterizan por:
		1)Tener un apuntador a un Array.
		2)Su tamaño es dinámico.
		3)Poseer una capacidad

	*/
	// nulo=null(vacio) en go es nil
	matriz := []int{1, 2, 3, 4}
	fmt.Println(matriz)
	matriz1 := []string{"hola", " que", " tal", " soy", " Giovanny."}
	fmt.Println(matriz1)

	fmt.Println("************** THEN END *******************")
	//////////////////////////////////////////////////////////
}
func clase8() {
	/////////////////////////////  Make y Append( parte de slice)  /////////////////////////////

	slice_int := make([]int, 4, 8) // make es un metodo q tiene (tipo de dato, longitud, capacidad)
	fmt.Println(slice_int)
	fmt.Println("longitud1:", len(slice_int))
	fmt.Println("capacidad1:", cap(slice_int))
	slice_int = append(slice_int, 2) // agrego otro slice, al agregar la longitud cambia su tamaño pero la capacidad no.
	fmt.Println(slice_int)
	fmt.Println("longitud2:", len(slice_int))
	fmt.Println("capacidad2:", cap(slice_int))

	slice_strn := make([]string, 3)
	fmt.Println(slice_strn)
	fmt.Println("longitud3:", len(slice_strn))
	fmt.Println("capacidad3", cap(slice_strn))
	slice_strn = append(slice_strn, " ", " ") // se agraga 2 slice, en total long=3+2
	fmt.Println(slice_strn)
	fmt.Println("longitud2:", len(slice_strn))
	fmt.Println("capacidad2:", cap(slice_strn))

	fmt.Println("************** THEN END *******************")
	//////////////////////////////////////////////////////////
}
func clase9() {
	/////////////////////////////  Copy  /////////////////////////////
	// la idea es pasar de un arreglo a un slace araves del metodo copy
	slice1 := []int{1, 2, 3, 4}
	copia1 := make([]int, 6)
	copy(copia1, slice1) // lo q hace aca es hacer una copia minima de elementos de slice a copia1.
	fmt.Println("1°", slice1)
	fmt.Println("1°", copia1)

	slice2 := []int{11, 22, 33}
	copia2 := make([]int, 4)
	copy(slice2, copia2) // lo q hace aca es hacer una copia minima de elementos de copia1 a slice.
	fmt.Println("2°", slice2)
	fmt.Println("2°", copia2)

	slice3 := []int{11, 22, 33, 44, 55, 66}
	copia3 := make([]int, len(slice3))
	copy(copia3, slice3) // lo q hace aca es hacer una copia minima de elementos de slice a copia1.
	fmt.Println("3°", slice3)
	fmt.Println("3°", copia3)

	slice_str := []string{"hola", "mundo"}
	copia_str := make([]string, 1)

	copy(copia_str, slice_str)
	fmt.Println("4°", slice_str)
	fmt.Println("4°", copia_str)

	fmt.Println("************** THEN END *******************")
	//////////////////////////////////////////////////////////
}
func clase10() {
	/////////////////////////////  Punteros  /////////////////////////////
	/* Un puntero es un tipo de datos cuyo valor es una dirección de memoria.
	1. Es una direccion de memoria
	2. en lugar de valor tenemos la direccion en la q esta el valor.
	ej: x,y => as123d => 5
		x => as123d => 6
		y ¿? => 6
		*T => *int  *string *struct
		ela valor cero en memoria es nulo
	*/
	var x, y, z *int // declaro a la variable x e y del tipo entero de direccionde memoria
	entero := 5
	enterZ := 6
	x = &entero // accede a la direccion de memoria  el la direccion de memoria
	y = &entero
	z = &enterZ

	fmt.Println("Direc.Memoria x: ", x) // imprime la direccion de memoria 0x10c10090
	fmt.Println("Direc.Memoria y: ", y) // imprime la direccion de memoria 0x10c10090
	fmt.Println("Direc.Memoria z: ", z) // nil= nulo

	fmt.Println("valor x: ", *x) // imprime el valor (puntero)
	fmt.Println("valor y: ", *y)
	fmt.Println("valor z: ", *z)

	*x = 10                       // aunq le cambie el valor mantiene su direccion.
	fmt.Println("Dir.Mem x: ", x) // 0x10c10090
	fmt.Println("val x: ", *x)    // 10

	fmt.Println("************** THEN END *******************")
	//////////////////////////////////////////////////////////
}

type User struct {
	edad             int    // el nul de int es cero (0)
	nombre, apellido string // el nul de string es un espacio vacio
}

func clase11() {
	/////////////////////////////  Structs  /////////////////////////////
	var uriel User
	fmt.Println("1°", uriel)
	fmt.Println("2°", User{nombre: "gio ", apellido: "aguilar ", edad: 24})
	/* se puede esto igual; uriel:= User{nombre: "gio ", apellido: "aguilar ", edad: 24} y se imprime uriel*/
	fmt.Println("3°", User{24, " Giovanny ", "Aguilar Rojas"}) // se puede asignar e imprimer segun el orden declarado

	uriel2 := new(User) // imstamcio un objeto y lo guardo en una memoria
	uriel2.nombre = "Giovanny"
	uriel2.apellido = " AguilarRojas"
	uriel2.edad = 24

	fmt.Println("direccion de memoria: ", uriel2) // retorna la direccion de memoria donde el objeto esta guardado.
	fmt.Println("nom: ", uriel2.nombre)           // aca te da el valor de los atrivutos , pero como no esta inicializado x default tira vacio
	fmt.Println("ape: ", uriel2.apellido)         // lo mismo con apellido
	fmt.Println("edad: ", uriel2.edad)            // en el cado de edad cero (0)
	fmt.Println("Otra forma::", (*uriel2).edad)

	fmt.Println("************** THEN END *******************")
	//////////////////////////////////////////////////////////
}
func (Usuario User) nombre_completo() string { // el this es un nombre q yo lo puce podria ser cualquer cosa
	return Usuario.nombre + " " + Usuario.apellido // esta metodo es similar a un costrunctor
}
func (Usuario User) edad_completo() int {
	return Usuario.edad
}
func clase12() {
	/////////////////////////////  Metodos de estructuras  /////////////////////////////
	obj := new(User)
	obj.nombre = "kevin"
	obj.apellido = "rojas"
	obj.edad = 5 

	fmt.Println(obj.nombre_completo(), obj.edad_completo())

	fmt.Println("************** THEN END *******************")

	//////////////////////////////////////////////////////////
}
func main() {
				// https://www.youtube.com/watch?v=rF3VP10S9SM&t=223s
	//clase1()
	//clase2()
	//clase3()
	//clase4()
	//clase5()
	//clase6()
	//clase7()
	//clase8()
	//clase9()
	//clase10()
	//clase11()
	clase12()

}

/*
The commands are:

        bug         start a bug report
        build       compile packages and dependencies
        clean       remove object files and cached files
        doc         show documentation for package or symbol
        env         print Go environment information
        fix         update packages to use new APIs
        fmt         gofmt (reformat) package sources
        generate    generate Go files by processing source
        get         download and install packages and dependencies
        install     compile and install packages and dependencies
        list        list packages or modules
        mod         module maintenance
        run         compile and run Go program
        test        test packages
        tool        run specified go tool
        version     print Go version
        vet         report likely mistakes in packages

Use "go help <command>" for more information about a command.

Additional help topics:

        buildmode   build modes
        c           calling between Go and C
        cache       build and test caching
        environment environment variables
        filetype    file types
        go.mod      the go.mod file
        gopath      GOPATH environment variable
        gopath-get  legacy GOPATH go get
        goproxy     module proxy protocol
        importpath  import path syntax
        modules     modules, module versions, and more
        module-get  module-aware go get
        module-auth module authentication using go.sum
        module-private module configuration for non-public modules
        packages    package lists and patterns
        testflag    testing flags
        testfunc    testing functions

Use "go help <topic>" for more information about that topic.
*/
